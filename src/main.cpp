//**********************************************************
//
// This program implements the RSA Encryption algorithm
// (32-bit) to produce a Cipher text from a user supplied
// plain text (input).
//**********************************************************

#include <iostream>
#include <cstring>
//#include <array>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <cstdlib> // Used for random number generation (RAND_MAX)
#include <math.h>

using namespace std;

template <typename T, std::size_t N> inline
std::size_t getSize( T(&)[N] ) {return N;}


/* This function generates a random integer greater than 100 000 000 */
int intRand() {
  int outputRandom = rand()%30;
  return outputRandom;
}

/* This function tests an integer for primality.
   It test for equal divisibility by 2 first. If the number is even, it cannot be prime.
   Then it incrementally checks for divisibility by odd integers starting with 3.
   It casts the integer as a double and takes it's sqrt to reduce the number of values that need to be checked.
   If no odd divisors are found, the test passes and the integer is deemed prime.
   This method was used to avoid the complexity of large numbers. Since a small integer to a small power can very
   quickly become a large number, I decided to use this method. The prefered method would be to use the Miller-Rabin
   method (or Fermat's little theorem) ( (a^p-1)mod p = 1 ) */

bool primalityTest (int inputForTest)
{
    bool result = true; // Set a Boolean variable to hold our result. Assume prime
    if (inputForTest % 2 == 0) {
        result = false;
    }
    else {
        int divisor = 3; // Set divisor to 3
        double inputForTest_d = static_cast<double>(inputForTest); // Create a double and cast integer input as double
        int upperLimit = static_cast<int>(sqrt(inputForTest_d) +1); // Prevent integer overflow

        while (divisor <= upperLimit)  // While divisor is less than the upper integer limit
        {
            if (inputForTest % divisor == 0) // If input value mod divisor is 0, the number cannot be prime (is divisible)
               {
                   result = false;
               }
             divisor +=2; // Increment the divisor by 2
        }
    }
    return result;
}


// This function finds te GCD of two integers.

int gcd(int a, int b)
{
  int c = a % b;
  while(c != 0)
  {
    a = b;
    b = c;
    c = a % b;
  }
  return b;
}

// This function uses the Extended Euclidean Algorithm to find the
// multiplicative inverse of e


pair<int, pair<int, int> > extendedEuclid(int a, int b) {
	int x = 1, y = 0;
	int xLast = 0, yLast = 1;
	int q, r, m, n;
	while(a != 0) {
		q = b / a;
		r = b % a;
		m = xLast - q * x;
		n = yLast - q * y;
		xLast = x, yLast = y;
		x = m, y = n;
		b = a, a = r;
	}
	return make_pair(b, make_pair(xLast, yLast));
}

int fetchDalt(int a, int m) {
    return (extendedEuclid(a,m).second.first + m) % m;
}

// This function returns a value for e such that gcd(e, phi(n) = 1 where 1<e<phi(n)
// A starting value for e is chosen randomly. The value is then tested against the
// equation. If it fails, e is decremented. If it passes, e is returned.
int fetchE(int phi_n)
{
    int myE = rand() % phi_n;
    while (!(gcd(myE, phi_n) == 1 ))
    {
        myE --;
    }
    return myE;
}

void generateKeys(int& e, int& d, int& n, int& x, int& y, int& gcd, int& phi_n)
{
    int p = 0;
    int q = 0;
    p = intRand();
    q = intRand();
    while (!(primalityTest(p))) {
           p++;
    }
    while (!(primalityTest(q))) {
           q++;
    }
    n = p*q;
    phi_n = ((p-1) * (q-1));
    e = fetchE(phi_n);
    d=fetchDalt(e, phi_n);
}

void getMessage(char inputMessage[], int &sizeOfM)
{
    //char localMessage[1024];

    cin.clear();
    fflush(stdin);

    cout << "Please enter the plain text message (Note: Maximum 1024 characters)" << endl;
    cin.getline(inputMessage,1024);

    int size = strlen(inputMessage);
    sizeOfM = size;
}

void encodeMessage(char inputMessage[], int encodedMessage[], int &sizeOfM)
{
    for (int count = 0; count < sizeOfM; count ++)
    {
        encodedMessage[count] = (int)inputMessage[count];
    }
}

void decodeMessage(int encryptedMessage[], char cipherText[], int &sizeOfM)
{
    for (int count = 0; count < sizeOfM; count ++)
    {
        cipherText[count] = (char)encryptedMessage[count];
    }
}
void menu(int& e, int& d, int& n) {

    //system("CLS");
        if (e>0) { cout << "Public Key:  (" << n << "," << e << ")" << endl; } else { cout << endl; }
        if (d>0) { cout << "Private Key: (" << n << "," << d << ")" << endl; } else { cout << endl; }


    cout << endl;
    cout << "[1] Generate Keys." << endl;
    cout << "[2] Input A Message." << endl;
    cout << "[3] Encrypt." << endl;
    cout << "[4] Decrypt." << endl;
    cout << "[5] View Keys." << endl;
    cout << "[0] Exit." << endl;
    cout << endl << "Choose:";
}

/*void encryptRSA(int e, int n, int encodedMessage[], int encryptedMessage[], int size)
{
    cout << "Encrypting message of length: " << size << endl;
    double e_d = static_cast<double>(e);
    double n_d = static_cast<double>(n);

    for (int count = 0; count < size; count ++)
    {
        double input_d = encodedMessage[count] * 1.0;
        cout << "input_d " << input_d;
        double mynum = pow(input_d, e_d);
        cout << " mynum " << mynum;
        mynum = fmod(mynum, n_d);
        cout << " mynum " << mynum;
        encryptedMessage[count] = mynum;
    }
}
*/
void encryptRSA(int e, int n, int encodedMessage[], int encryptedMessage[], int size)
{
    cout << "Encrypting message of length: " << size << endl;

    long double e_d = e;
    long double n_d = n;

    for (int count = 0; count < size; count ++)
    {
        int input = encodedMessage[count];
        long double mynum = pow(input, e_d);
        mynum = fmod(mynum,n_d);
        cout << mynum << " ";
        //encryptedMessage[count] = mynum;
    }
}
void decryptRSA(int d, int n, int encodedMessage[], int decryptedMessage[], int size)
{
    cout << "Decrypting message of length: " << size << endl;
    double d_d = static_cast<double>(d);
    double n_d = static_cast<double>(n);

    for (int count = 0; count < size; count ++)
    {
        double input_d = encodedMessage[count] * 1.0;
        long double mynum = pow(input_d, d_d);
        mynum = fmod(mynum, n);
        decryptedMessage[count] = mynum;
    }
}

int main()
{
    int e = 0;
    int d = 0;
    int n = 0;
    int phi_n = 0;
    int x = 0;   // Used for EEA to modify by reference
    int y = 0;   // Used for EEA to modify by reference
    int gcd = 0; // Used for EEA to modify by reference
    int choice = 0;
    int size = 0;

    char* inputMessage = new char[1024];
    int* encodedMessage = new int[1024];
    int* encryptedMessage = new int[1024];
    int* decryptedMessage = new int[1024];
    char* cipherText = new char[1024];
    char* plainText = new char[1024];

    bool keys = false;
    bool message = false;
    bool encoded = false;
    bool encrypted = false;
    bool decoded = false;
    bool decrypted = false;


    bool choseExit = false;

    /* Initialize Random Seed */
    srand ( time (NULL) );

    while (!(choseExit))
    {

        if (message) { cout << "inputMessage: " << inputMessage << endl; }
        if (encoded) {
        cout << "Your encoded Message: ";
        for (int count = 0; count < size; count ++)
        {
            cout << encodedMessage[count] << " ";
        }
        cout << endl;
        }

        if (encrypted) {
        cout << "Your encrypted Message: ";
        for (int count = 0; count < size; count ++)
        {
            cout << encryptedMessage[count] << " ";
        }
        cout << endl;
        }

        if (decoded) {
        cout << "Your dec/enc Message: ";
        for (int count = 0; count < size; count ++)
        {
            cout << cipherText[count];
        }
        cout << endl;
        }

        if (decrypted) {
            cout << "Your decrypted Message: ";
            for (int count = 0; count < size; count ++)
            {
                cout << decryptedMessage[count] << " ";
            }
            cout << endl;
        }


        menu(e, d, n);
        cin >> choice;

        if (choice == 1) {
            // Generate a set of Public and Private keys
            generateKeys(e, d, n, x, y, gcd, phi_n);
            while ((n>200) && (e>200)) {
                 generateKeys(e, d, n, x, y, gcd, phi_n);
            if ( d == e) {
                cout << "Bad Key Pair. Regenerate!" << endl;
            } else { keys = true; }
        }
        }
        else if (choice == 2)
        {
            //message = getMessage(size); // Get message as an Array of Integers (Ascii codes)
            getMessage(inputMessage, size);
            message = true;
            encodeMessage(inputMessage, encodedMessage, size);
            encoded = true;

        }
        else if (choice == 3)
        {
           encryptRSA(e, n, encodedMessage, encryptedMessage, size);
           encrypted = true;
           decodeMessage(encryptedMessage, cipherText, size);
           decoded = true;
        }
        else if (choice == 4)
        {
           decryptRSA(d, n, encodedMessage, decryptedMessage, size);

            decodeMessage(decryptedMessage, plainText, size);
            cout << "Your dec/denc Message: ";
            for (int count = 0; count < size; count ++)
            {
                cout << plainText[count];
            }
            cout << endl;
        }
        else if (choice == 0)
        {
            choseExit = true;
        }
        else
        {
            cout << "Invalid choice." << endl;
        }
    }
}







